package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"math"
	"os"
	"runtime"
	"runtime/pprof"
	"slices"
	"strings"
)

var cpuprofile = flag.String("cpuprofile", "", "Write CPU profile to file")

type station struct {
	min   int
	max   int
	sum   int
	count int
}

func digitToInt(b byte) int {
	return int(b - '0')
}

func addEntry(m map[string]*station, name []byte, n int) {
	data, ok := m[string(name)]
	if !ok {
		m[string(name)] = &station{
			min:   n,
			max:   n,
			sum:   n,
			count: 1,
		}
		return
	}
	if n < data.min {
		data.min = n
	}
	if n > data.max {
		data.max = n
	}
	data.sum += n
	data.count += 1
}

func processChunk(buf []byte, m map[string]*station) {
	for {
		semiIdx := bytes.IndexByte(buf, ';')
		if semiIdx < 0 {
			break
		}
		name := buf[:semiIdx]
		num := buf[semiIdx+1:]
		end := semiIdx + 1
		negative := false
		if num[0] == '-' {
			negative = true
			num = num[1:]
			end++
		}
		var n int
		if num[1] == '.' {
			n = 10*digitToInt(num[0]) + digitToInt(num[2])
			end += 3
		} else {
			n = 100*digitToInt(num[0]) + 10*digitToInt(num[1]) + digitToInt(num[3])
			end += 4
		}
		if negative {
			n = -n
		}
		addEntry(m, name, n)
		buf = buf[end+1:]
	}
}

func process(results chan<- map[string]*station) {
	defer close(results)
	maxConcurrency := runtime.NumCPU()
	buffers := make(chan []byte, maxConcurrency)
	const bufSize = 4 * 1024 * 1024
	for range maxConcurrency {
		buffers <- make([]byte, bufSize)
	}
	buf := make([]byte, bufSize)
	leftoverLen := 0
	for {
		n, err := os.Stdin.Read(buf[leftoverLen:])
		if err != nil || n <= 0 {
			break
		}
		buf = buf[:leftoverLen+n]
		lastLineIdx := bytes.LastIndexByte(buf, '\n')
		nextBuf := <-buffers
		nextBuf = nextBuf[:cap(nextBuf)]
		copy(nextBuf, buf[lastLineIdx+1:])
		leftoverLen = len(buf) - (lastLineIdx + 1)
		go func(chunk []byte) {
			m := make(map[string]*station, 5000)
			processChunk(chunk, m)
			results <- m
			buffers <- chunk
		}(buf[:lastLineIdx+1])
		buf = nextBuf
	}
	for range maxConcurrency {
		<-buffers
	}
}

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %v\n", err)
			os.Exit(1)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	results := make(chan map[string]*station)
	go process(results)

	m := make(map[string]*station)
	for rs := range results {
		for name, newData := range rs {
			data, ok := m[name]
			if !ok {
				m[name] = newData
				continue
			}
			if newData.min < data.min {
				data.min = newData.min
			}
			if newData.max > data.max {
				data.max = newData.max
			}
			data.sum += newData.sum
			data.count += newData.count
		}
	}

	type result struct {
		name    string
		station *station
	}
	names := make([]result, 0, len(m))
	for name, station := range m {
		names = append(names, result{
			name:    name,
			station: station,
		})
	}
	slices.SortFunc(names, func(x, y result) int {
		return strings.Compare(x.name, y.name)
	})
	out := bufio.NewWriter(os.Stdout)
	defer out.Flush()
	fmt.Fprint(out, "{")
	for i, result := range names {
		if i > 0 {
			fmt.Fprint(out, ", ")
		}
		fmt.Fprintf(out, "%s=%.1f/%.1f/%.1f", result.name, float64(result.station.min)/10, math.RoundToEven(float64(result.station.sum)/float64(result.station.count))/10, float64(result.station.max)/10)
	}
	fmt.Fprint(out, "}\n")
}
